import React, { Component } from 'react';
import { Provider, Divider } from 'rebass';
import { injectGlobal } from 'styled-components';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import logo from './logo.png';
import './App.css';
import Home from './components/Home';
import Result from './components/Result';

injectGlobal`
  * { box-sizing: border-box; }
  body { margin: 0; }
`;

class App extends Component {
  render() {
    return (
      <Provider>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <Divider w={3} borderColor="blue" mb={50} />
          </header>
          <Router>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/result" component={Result} />
            </Switch>
          </Router>
        </div>
      </Provider>
    );
  }
}

export default App;
