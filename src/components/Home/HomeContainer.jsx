import React, { Component } from 'react';
import { searchLocation } from '../../utilities';
import Search from './Search';
import Results from './Results';

class HomeContainer extends Component {
  state = { input: '', results: [], loaded: false };

  handleInput = e =>
    this.setState({
      input: e.target.value,
    });

  search = () => {
    this.state.input &&
      searchLocation(this.state.input).then(res =>
        this.setState({ results: res, loaded: true })
      );
  };

  render = () => (
    <div>
      <Search handleInput={this.handleInput} handleSearch={this.search} />
      <Results results={this.state.results} loaded={this.state.loaded} />
    </div>
  );
}

export default HomeContainer;
