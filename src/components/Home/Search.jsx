import React from 'react';
import PropTypes from 'prop-types';
import { Heading, Input, Text, Flex, Button } from 'rebass';

const handleKeyPress = search => e => {
  var code = e.keyCode || e.which;

  if (code === 13) {
    search();
  }
};

const Search = ({ handleInput, handleSearch }) => (
  <Flex py={20} flexDirection="column">
    <Heading mb={20}>MetaWeather search</Heading>
    <Flex justifyContent="center" alignItems="center" px={20}>
      <Text fontWeight="bold" mr={20}>
        Search:
      </Text>
      <Input
        onChange={handleInput}
        placeholder="location"
        width={[1, 0.5]}
        mr={20}
        onKeyPress={handleKeyPress(handleSearch)}
      />
      <Button onClick={handleSearch}>Go</Button>
    </Flex>
  </Flex>
);

Search.propTypes = {
  handleInput: PropTypes.func.isRequired,
  handleSearch: PropTypes.func.isRequired,
};

export default Search;
