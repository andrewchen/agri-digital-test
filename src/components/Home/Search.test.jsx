import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Button } from 'rebass';
import Search from './Search';

it('renders correctly', () => {
  const wrapper = shallow(
    <Search handleInput={jest.fn()} handleSearch={jest.fn()} />
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});

it('calls search function when button is clicked', () => {
  const mockSearch = jest.fn();
  const wrapper = shallow(
    <Search handleInput={jest.fn()} handleSearch={mockSearch} />
  );

  wrapper.find(Button).simulate('click');
  expect(mockSearch.mock.calls.length).toBe(1);
});
