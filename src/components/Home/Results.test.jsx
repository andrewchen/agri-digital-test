import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Results from './Results';

const mockResults = [
  {
    title: 'Berlin',
    location_type: 'City',
    woeid: 638242,
    latt_long: '52.516071,13.376980',
  },
  {
    title: 'Nuremberg',
    location_type: 'City',
    woeid: 680564,
    latt_long: '49.454342,11.07349',
  },
  {
    title: 'Aberdeen',
    location_type: 'City',
    woeid: 10243,
    latt_long: '57.153820,-2.106790',
  },
];

it('renders error message when there are no results', () => {
  const wrapper = shallow(<Results loaded results={[]} />);

  expect(toJson(wrapper)).toMatchSnapshot();
});

it('renders correctly with results', () => {
  const wrapper = shallow(<Results loaded results={mockResults} />);

  expect(toJson(wrapper)).toMatchSnapshot();
});
