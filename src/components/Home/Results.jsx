import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box, Column, Row } from 'rebass';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const StyledLink = styled(Link)`
  text-decoration: none;
  color: inherit;

  :hover {
    color: gray;
  }
`;

const Results = ({ results, loaded }) => (
  <Flex justifyContent="center">
    <Box width={1 / 2}>
      {loaded && results.length === 0 && <div>No results!</div>}
      {results &&
        results.map(result => (
          <StyledLink key={result.woeid} to={`result?location=${result.woeid}`}>
            <Row>
              <Column width={1 / 3}>{result.title}</Column>
              <Column width={1 / 3}>{result.location_type}</Column>
              <Column width={1 / 3}>{result.latt_long}</Column>
            </Row>
          </StyledLink>
        ))}
    </Box>
  </Flex>
);

Results.propTypes = {
  results: PropTypes.array.isRequired,
  loaded: PropTypes.bool.isRequired,
};

export default Results;
