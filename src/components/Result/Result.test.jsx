import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Image } from 'rebass';
import Result from './Result';

const mockResult = {
  consolidated_weather: [
    {
      id: 5902253592412160,
      weather_state_name: 'Light Cloud',
      weather_state_abbr: 'lc',
      wind_direction_compass: 'NNW',
      created: '2018-08-11T21:54:11.992200Z',
      applicable_date: '2018-08-12',
      min_temp: 23.826666666666668,
      max_temp: 28.213333333333335,
      the_temp: 27.170000000000002,
      wind_speed: 20.8491289,
      wind_direction: 340.50034287084117,
      air_pressure: 1013,
      humidity: 63,
      visibility: 14.042367573371511,
      predictability: 70,
    },
    {
      id: 5995784625455104,
      weather_state_name: 'Light Cloud',
      weather_state_abbr: 'lc',
      wind_direction_compass: 'NNW',
      created: '2018-08-11T21:54:15.049980Z',
      applicable_date: '2018-08-13',
      min_temp: 23.859999999999999,
      max_temp: 28.41,
      the_temp: 26.690000000000001,
      wind_speed: 21.018001733333332,
      wind_direction: 336.83333803442827,
      air_pressure: 1011,
      humidity: 65,
      visibility: 13.944812296190248,
      predictability: 70,
    },
  ],
  time: '2018-08-12T03:49:07.873030+03:00',
  sun_rise: '2018-08-12T06:33:19.568316+03:00',
  sun_set: '2018-08-12T20:12:36.358588+03:00',
  timezone_name: 'LMT',
  title: 'Santorini',
  location_type: 'City',
  woeid: 56558361,
  latt_long: '36.406651,25.456530',
  timezone: 'Europe/Athens',
};

it('renders correctly', () => {
  const wrapper = shallow(<Result data={mockResult} />);

  expect(toJson(wrapper)).toMatchSnapshot();
});

it('renders an image for each weather object', () => {
  const wrapper = shallow(<Result data={mockResult} />);

  expect(wrapper.find(Image)).toHaveLength(
    mockResult.consolidated_weather.length
  );
});
