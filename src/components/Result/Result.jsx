import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box, Image, Heading, Button } from 'rebass';
import { Link } from 'react-router-dom';
import { getImageUrl } from '../../constants';

const roundNumber = n => Number.parseFloat(n).toFixed(2);

const Result = ({ data: { title, consolidated_weather } }) => (
  <Flex justifyContent="center" flexDirection="column">
    <Heading>{title}</Heading>
    {consolidated_weather &&
      consolidated_weather.map(w => (
        <Flex px={50} key={w.id} justifyContent="center">
          <Image width={100} src={getImageUrl(w.weather_state_abbr)} pr={20} />
          <Flex pt={30} flexDirection="column">
            <Box>{w.applicable_date}</Box>
            <Box>
              Min: {roundNumber(w.min_temp)}
              &#8451;
            </Box>
            <Box>
              Max: {roundNumber(w.max_temp)}
              &#8451;
            </Box>
            <Box>Humidity: {w.humidity}</Box>
          </Flex>
        </Flex>
      ))}
    <Link to="/">
      <Button>Go back</Button>
    </Link>
  </Flex>
);

Result.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    consolidated_weather: PropTypes.array,
  }).isRequired,
};

export default Result;
