import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getLocationData } from '../../utilities';
import Result from './Result';

class ResultContainer extends Component {
  state = {
    data: null,
    loaded: false,
  };

  componentDidMount() {
    const {
      location: { search },
    } = this.props;

    const woeid = search && search.split('=')[1];

    getLocationData(woeid).then(res =>
      this.setState({ data: res, loaded: true })
    );
  }

  render() {
    if (!this.state.loaded) {
      return <div>Loading...</div>;
    }

    return <Result data={this.state.data} />;
  }
}

ResultContainer.propTypes = {
  location: PropTypes.shape({
    search: PropTypes.string,
  }).isRequired,
};

export default ResultContainer;
