const API_URL = 'https://www.metaweather.com';

export const getLocationSearchUrl = query =>
  `${API_URL}/api/location/search/?query=${query}`;

export const getLocationUrl = woeId => `${API_URL}/api/location/${woeId}/`;

export const getImageUrl = weatherState =>
  `${API_URL}/static/img/weather/${weatherState}.svg`;
