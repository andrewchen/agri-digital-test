import axios from 'axios';
import { getLocationSearchUrl, getLocationUrl } from './constants';

const getData = url =>
  axios
    .get(url)
    .then(res => res.data)
    .catch(err => err);

export const searchLocation = query => getData(getLocationSearchUrl(query));

export const getLocationData = query => getData(getLocationUrl(query));
