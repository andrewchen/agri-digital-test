# Agridigital UI Coding Test

Please make sure you read the entire instructions below before start coding. There is no time limit for this test, but it should not take more than a couple of hours.

Meta weather provides an API for Location Search. You can find the API documentation here: [metaweather-api-location](https://www.metaweather.com/api/#locationsearch)
Note that CORS has not been enabled on the server side, if you are using Chrome please use extension like [Allow-Control-Allow-Origin: \*](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en) for development only (may affect other sites).

For the purpose of this coding test, build a React application allowing users to search for locations.

### The final application should consist of:

- **A home page** where the user can search by location name (e.g "San") via a search box. The search results should be displayed as list. Each search result should display the title, location type, latt and long. Clicking on a location should open the details page described below.
- **An location page** which displays the location name, weather information, and anything else you find interesting.

### Requirements:

- You must use React
- You can use the state management library of your choice
- The application must be responsive
- Keep UI clean and simple, no pixel perfect is fine.
- Refrain from using any boilerplate other than create-react-app (and if you don't want to use any, that's fine with us!)
- You may develop only for Chrome
- Use any feature available in the latest stable release of Chrome

Important points:

- Unit tests (examples on how you test your code would be great!)
- Any other libraries/tools that you believe relevant for the purpose of this coding test

Once you are done and happy with your result, please send us the link to your github or bitbucket repository with commit history.

Good luck and have fun!
